package kg.kenguroo.recyclerview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class SecondActivity extends AppCompatActivity {

    Car car;

    private String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Log.d(TAG, "onCreate");

        car = (Car) getIntent().getParcelableExtra("car");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        ImageView iconFavorite = findViewById(R.id.favorite);
        ImageView imageView = findViewById(R.id.imageView);
        TextView textView = findViewById(R.id.description);

        setTitle(car.getName());

        iconFavorite.setSelected(car.isFavorite());
        Picasso.get().load(car.getImageUrl()).into(imageView);
        textView.setText(car.getDescription());
    }

    public void onFavoriteClick(View view) {
        car.setFavorite(!car.isFavorite());
        view.setSelected(car.isFavorite());

        Intent intent = new Intent();
        intent.putExtra("car", car);
        setResult(RESULT_OK, intent);
    }


    public void onEditButtonClick(View view) {
        startActivity(new Intent(this, EditCarActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
