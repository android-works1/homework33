package kg.kenguroo.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<ListViewHolder> {

    private ArrayList<Car> carList;
    private OnItemClickListener listener;

    public DataAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ListViewHolder(inflater.inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
        Car car = carList.get(position);
        holder.setCar(car);
        holder.itemView.setOnClickListener(view -> listener.onClick(carList.get(position)));

        holder.deleteBtn.setOnClickListener(view -> removeItem(position));

        holder.likeBtn.setOnClickListener(view -> itemChanged(position));
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public void addItem(Car car) {
        carList.add(car);
        notifyItemInserted(carList.size() - 1);
    }

    public void addItems(List<Car> cars) {
        carList.addAll(cars);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<Car> cars) {
        carList = cars;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        carList.remove(position);
        notifyItemRemoved(position);
    }

    public void itemChanged(int position) {
        carList.get(position).setFavorite(!carList.get(position).isFavorite());
        notifyItemChanged(position);
    }

    public ArrayList<Car> getItems() {
        return carList;
    }
}

class ListViewHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView title;
    TextView description;
    TextView price;
    ImageView deleteBtn, likeBtn;

    public ListViewHolder(@NonNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.imageView);
        title = itemView.findViewById(R.id.title);
        description = itemView.findViewById(R.id.description);
        price = itemView.findViewById(R.id.price);
        deleteBtn = itemView.findViewById(R.id.delete_btn);
        likeBtn = itemView.findViewById(R.id.like_btn);
    }

    public void setCar(Car car) {
        title.setText(car.getName());
        description.setText(car.getDescription());
        price.setText(String.valueOf(car.getPrice()));
        Picasso.get().load(car.getImageUrl()).into(imageView);
        likeBtn.setSelected(car.isFavorite());
    }
}