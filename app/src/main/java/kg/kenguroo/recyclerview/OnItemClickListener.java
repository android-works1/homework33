package kg.kenguroo.recyclerview;

public interface OnItemClickListener {
    void onClick(Car car);
}
