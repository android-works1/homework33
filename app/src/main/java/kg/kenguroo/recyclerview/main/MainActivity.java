package kg.kenguroo.recyclerview.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import kg.kenguroo.recyclerview.Car;
import kg.kenguroo.recyclerview.DataAdapter;
import kg.kenguroo.recyclerview.OnItemClickListener;
import kg.kenguroo.recyclerview.R;
import kg.kenguroo.recyclerview.SecondActivity;

public class MainActivity extends AppCompatActivity implements
        MainContract.View,
        OnItemClickListener,
        SearchView.OnQueryTextListener {

    static int REQUST_CODE_FOR_SECOND_ACTIVITY = 1;

    private DataAdapter adapter;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this);
        initViews();
        presenter.getCarList();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Car> cars = adapter.getItems();
        outState.putParcelableArrayList("cars", cars);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ArrayList<Car> cars = savedInstanceState.getParcelableArrayList("cars");
        showList(cars);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUST_CODE_FOR_SECOND_ACTIVITY && resultCode == RESULT_OK) {
            Car car = data.getParcelableExtra("car");
            presenter.updateCarList(car);
        }
    }

    private void initViews() {
        RecyclerView recyclerView = findViewById(R.id.recycleView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new DataAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showList(ArrayList<Car> cars) {
        adapter.resetItems(cars);
    }

    @Override
    public void showUpdatedList(ArrayList<Car> cars) {
        adapter.resetItems(cars);
    }

    @Override
    public void showErrorText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFilteredList(List<Car> cars) {
        adapter.resetItems(new ArrayList<>(cars));
    }

    @Override
    public void onClick(Car car) {
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("car", car);
        startActivityForResult(intent, REQUST_CODE_FOR_SECOND_ACTIVITY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optional_menu, menu);

        initSearchView(menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void initSearchView(Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.search_view).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("onQueryTextSubmit", query);
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onQueryTextChange(String newText) {
        presenter.onSearchTextChanged(newText);
        return false;
    }
}
