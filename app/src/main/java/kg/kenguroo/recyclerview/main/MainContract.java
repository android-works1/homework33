package kg.kenguroo.recyclerview.main;

import java.util.ArrayList;
import java.util.List;

import kg.kenguroo.recyclerview.Car;

public interface MainContract {
    interface View {
        void showList(ArrayList<Car> cars);
        void showFilteredList(List<Car> cars);
        void showUpdatedList(ArrayList<Car> cars);
        void showErrorText(String text);
    }

    interface Repository {
        ArrayList<Car> loadData();
        List<Car> filterData(String text);
    }

    interface Presenter {
        void getCarList();
        void updateCarList(Car car);
        void onSearchTextChanged(String text);
    }
}
