package kg.kenguroo.recyclerview.main;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;

import kg.kenguroo.recyclerview.Car;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private MainContract.Repository repository;

    private ArrayList<Car> carList = new ArrayList<>();

    public MainPresenter(MainContract.View view) {
        this.view = view;
        repository = new MainRepository();
    }

    @Override
    public void getCarList() {
        carList = repository.loadData();
        if (carList.size() == 0) {
            view.showErrorText("No data");
        } else {
            view.showList(carList);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onSearchTextChanged(String text) {
        view.showFilteredList(repository.filterData(text));
    }

    @Override
    public void updateCarList(Car car) {
        int index = carList.indexOf(car);
        carList.set(index, car);
        view.showUpdatedList(carList);
    }
}
