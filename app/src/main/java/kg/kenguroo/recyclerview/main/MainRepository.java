package kg.kenguroo.recyclerview.main;

import android.content.SharedPreferences;
import android.os.Build;
import androidx.preference.PreferenceManager;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import kg.kenguroo.recyclerview.App;
import kg.kenguroo.recyclerview.Car;

public class MainRepository implements MainContract.Repository {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public List<Car> filterData(String text) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());

        List<Car> cars = loadData().stream()
                .filter(car -> car.getName().toLowerCase().contains(text.toLowerCase()))
                .collect(Collectors.toList());
        return cars;
    }

    @Override
    public ArrayList<Car> loadData() {
        ArrayList carList = new ArrayList<>();

        carList.add(new Car("Tesla Model 3",
                "https://www.teslarati.com/wp-content/uploads/2019/06/tesla-model-3-rhd-right-hand-drive-e1562996501187.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Ferrari Patent",
                "https://cdn.motor1.com/images/mgl/7Me7q/s4/ferrari-halo.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Ford",
                "https://article.images.consumerreports.org/f_auto/prod/content/dam/CRO-Images-2020/Cars/01Jan/CR-Cars-InlineHero-2020-Toyota-Tacoma-f-1-20",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                350000));
        carList.add(new Car(" Mercedes S-Class",
                "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/large-2479-s-classsaloon.jpg?itok=QTxMln2k",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Tesla Model 3",
                "https://www.teslarati.com/wp-content/uploads/2019/06/tesla-model-3-rhd-right-hand-drive-e1562996501187.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Ferrari Patent",
                "https://cdn.motor1.com/images/mgl/7Me7q/s4/ferrari-halo.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car(" Mercedes S-Class",
                "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/large-2479-s-classsaloon.jpg?itok=QTxMln2k",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Tesla Model 3",
                "https://www.teslarati.com/wp-content/uploads/2019/06/tesla-model-3-rhd-right-hand-drive-e1562996501187.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Ferrari Patent",
                "https://cdn.motor1.com/images/mgl/7Me7q/s4/ferrari-halo.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car(" Mercedes S-Class",
                "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/large-2479-s-classsaloon.jpg?itok=QTxMln2k",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Tesla Model 3",
                "https://www.teslarati.com/wp-content/uploads/2019/06/tesla-model-3-rhd-right-hand-drive-e1562996501187.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car("Ferrari Patent",
                "https://cdn.motor1.com/images/mgl/7Me7q/s4/ferrari-halo.jpg",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        carList.add(new Car(" Mercedes S-Class",
                "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/large-2479-s-classsaloon.jpg?itok=QTxMln2k",
                "The UK’s new car registrations rankings from the Society of Motor Manufacturers and Traders (SMMT) in August 2019 proved quite surprising, thanks in no small part to the arrival and apparent success of the Tesla Model 3. ",
                35000));
        return carList;
    }
}
