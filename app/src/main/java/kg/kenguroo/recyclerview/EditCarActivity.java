package kg.kenguroo.recyclerview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class EditCarActivity extends AppCompatActivity {

    EditText editText;
    TextView textView;

    String emptyString = "text is empty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_car);

        editText = findViewById(R.id.edit_text);
        textView = findViewById(R.id.textView);

        textView.setText(emptyString);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(savedInstanceState.getString("text"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("text", textView.getText().toString());
    }

    public void onClick(View view) {
        if (editText.getText() == null || editText.getText().toString().isEmpty()) {
            textView.setText(emptyString);
            return;
        }
        textView.setText(editText.getText().toString());
    }
}
